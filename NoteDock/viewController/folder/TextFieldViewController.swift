//
//  AuthenticationViewController.swift
//  NoteDock
//
//  Created by Branislav Bily on 31/12/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import UIKit

class TextFieldViewController: UIViewController {
    
    //Hides keyboard once you tap outside of UITextField
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
}
