//
//  FolderViewController.swift
//  NoteDock
//
//  Created by Branislav Bily on 30/09/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import UIKit
import RxSwift
import FirebaseAuth
import Crashlytics

class FolderViewController: UIViewController {
    
    var notes: [NoteModel] = []
    @IBOutlet var tableView: UITableView!
    
    var folder: FolderModel!
    var lastTime: Int = 0
    var throttleTimeOut: Int = 750
    
    var notesFromFirebase: LoadNotesFromFirebaseUseCase!
    var dialogRepo: DialogRepository!
    var cellRepo: CellRepository!
    
    var folderViewViewModel: FolderViewViewModelProtocol!
    
    override func viewDidLoad() {
        self.title = folder.name
        
        if let uid = folder.uid {
            folderViewViewModel.loadNotes(folderUUID: uid)
        }
        setupDataBinding()
        
    }
    
    //MARK: User state
     override func viewWillAppear(_ animated: Bool) {
        if let user = Auth.auth().currentUser {
            user.reload(completion: { error in
                if error != nil {
                    self.performSegue(withIdentifier: CSegue.FolderToLogin, sender: nil)
                }
            })
        }
     }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "folderToNoteSeg" {
            let noteUUID = sender as! String
            
            let vc = segue.destination as! NoteViewController
            vc.folderUUID = folder.uid
            vc.noteUUID = noteUUID
        }
    }
    @IBAction func addNoteButtonPressed(_ sender: Any) {
        //Throttling button
        if let uid = folder.uid {
            let currentTime = Int(Date().timeIntervalSince1970 * 1000)
            if (currentTime - lastTime > throttleTimeOut) {
                lastTime = currentTime
                folderViewViewModel.addNote(folderUUID: uid)
            }
        }
    }
    
    @IBAction func unwindToFolder(_ unwindSegue: UIStoryboardSegue) {
        _ = unwindSegue.source
    }
    
    private func setupDataBinding() {
        
        folderViewViewModel
            .notesLoaded
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { loadNotesEvent in
                switch loadNotesEvent {
                    case .Success(let notes): do {
                        self.notes = notes
                        self.tableView.reloadData()
                    }
                    case .Error(let error): do {
                        switch error {
                            case .UnknownError: self.dialogRepo.showUnknownErrorDialog(viewController: self)
                        }
                    }
                }
            })
        
        folderViewViewModel
            .noteAdded
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { addNoteEvent in
                switch addNoteEvent {
                    case .Success(let noteUUID): do {
                            self.prepareSegToNote(noteUUID: noteUUID)
                        }
                    case .Error(let error): do {
                        switch error {
                            case .UnknownError: self.dialogRepo.showUnknownErrorDialog(viewController: self)
                        }
                    }
                }
            })
        
        folderViewViewModel
            .noteDeleted
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { deleteNoteEvent in
                switch deleteNoteEvent {
                case .Success: do {
                    self.tableView.reloadData()
                    }
                    case .Error(let error): do {
                        switch error {
                            case .NoUserFound: self.showNoUserFoundDialog()
                            case .UnknownError: self.dialogRepo.showUnknownErrorDialog(viewController: self)
                        }
                    }
                }
        })
    }
    
    //MARK: Dialog
    private func deleteNoteDialog(indexPath: IndexPath) {
        dialogRepo.showDeleteNoteDialog(viewController: self, completion: {
            guard let folderUUID = self.folder.uid else {
                print("FolderUUID is nil")
                return
            }
            guard let noteUUID = self.notes[indexPath.row].uid else {
                print("NoteUUID is nil")
                return
            }
            self.folderViewViewModel.deleteNote(folderUUID: folderUUID, noteUUID: noteUUID)
        })
    }
    
    private func showNoUserFoundDialog() {
        self.dialogRepo.showNoUserFound(viewController: self, completion: {
            self.performSegue(withIdentifier: CSegue.FolderToLogin, sender: nil)
        })
    }
    
    private func prepareSegToNote(noteUUID: String) {
        self.performSegue(withIdentifier: CSegue.FolderToNote, sender: noteUUID)
    }
}

extension FolderViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.cellRepo.buildNoteCell(indexPath: indexPath)
        cell.bindData(noteModel: self.notes[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action ,indexPath) in
            self.deleteNoteDialog(indexPath: indexPath)
        }
        return [delete]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let noteUUID = self.notes[indexPath.row].uid {
            self.prepareSegToNote(noteUUID: noteUUID)
        }
    }
}
