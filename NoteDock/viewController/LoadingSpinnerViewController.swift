//
//  LoadingSpinnerViewController.swift
//  NoteDock
//
//  Created by Branislav Bily on 02/01/2020.
//  Copyright © 2020 Pixel Art. All rights reserved.
//

import UIKit

class LoadingSpinnerViewController: TextFieldViewController {
    
    var spinnerView: UIView?
    
    //MARK: Loading spinner
    func showLoading(view: UIView) {
        spinnerView = UIView.init(frame: view.bounds)
        if let spinner = spinnerView {
            let loadingIndicator = UIActivityIndicatorView.init(style: .whiteLarge)
            loadingIndicator.color = UIColor.init(named: "SpinnerColor")
            loadingIndicator.startAnimating()
            loadingIndicator.center = spinner.center
            spinner.addSubview(loadingIndicator)
            view.addSubview(spinner)
        }
    }
    
    func dismissLoading() {
        spinnerView?.removeFromSuperview()
        spinnerView = nil
    }
}
