//
//  SettingsViewController.swift
//  NoteDock
//
//  Created by Branislav Bily on 31/12/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import UIKit
import FirebaseAuth

class SettingsViewController: UIViewController {
    
    var settingsViewModel: SettingsViewViewModelProtocol!
    var cellRepo: CellRepository!
    
    @IBOutlet var tableView: UITableView!
    var handle: AuthStateDidChangeListenerHandle?
    
    //MARK: UserState
    override func viewWillAppear(_ animated: Bool) {
        handle = Auth.auth().addStateDidChangeListener { (auth, user) in
            if user != nil {
                self.tableView.reloadData()
            } else {
                self.performSegue(withIdentifier: CSegue.SettingsToLogin, sender: nil)
            }
        }
     }
        
    override func viewWillDisappear(_ animated: Bool) {
        Auth.auth().removeStateDidChangeListener(handle!)
    }
    
    private func logOut() {
        if Auth.auth().currentUser != nil {
            do {
                try Auth.auth().signOut()
            }
            catch {}
        }
    }
    
    //MARK: Dialog
    private func showDialogRateUs() {
        let alert = UIAlertController(title: CDialogTitle.RateUs, message: CDialogMessage.RateUs, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: CAction.Ok, style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

extension SettingsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 2
        } else if section == 1 {
            return 2
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = self.cellRepo.buildSettingsCell(indexPath: indexPath)
            var title = ""
            switch indexPath.row {
                case 0:do {
                    title = "Account"
                    let cell = self.cellRepo.buildAccountDetailCell(indexPath: indexPath)
                    cell.bindData(profileImage: UIImage(named: "logoGradient"))
                    return cell
                }
                default : title = "Change password"
            }
            cell.bindData(title: title)
            return cell
        } else if indexPath.section == 1 {
            let cell = self.cellRepo.buildSettingsCell(indexPath: indexPath)
            var title = ""
            switch indexPath.row {
                case 0: title = "Rate us"
                default : title = "Help and support"
            }
            cell.bindData(title: title)
            return cell
        } else {
            let cell = self.cellRepo.buildSettingsActionCell(indexPath: indexPath)
            cell.bindData(title: "Log out")
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
            case 0: do {
                switch indexPath.row {
                    case 0: self.performSegue(withIdentifier: CSegue.SettingsToAccount, sender: nil)
                    default: self.performSegue(withIdentifier: CSegue.SettingsToChangePassword, sender: nil)
                }
            }
            case 1: do {
                if (indexPath.row == 0) {
                    self.showDialogRateUs()
                } else {
                    self.performSegue(withIdentifier: CSegue.SettingsToHelp, sender: nil)
                }
            }
            default: logOut()
        }
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
}
