//
//  ChangePasswordViewController.swift
//  NoteDock
//
//  Created by Branislav Bily on 01/01/2020.
//  Copyright © 2020 Pixel Art. All rights reserved.
//

import UIKit
import RxSwift
import FirebaseAuth

class ChangePasswordViewController: LoadingSpinnerViewController {
    
    var changePasswordViewModel: ChangePasswordViewViewModelProtocol!
    var dialogRepo: DialogRepository!
    
    @IBOutlet var buttonSave: UIBarButtonItem!
    
    @IBOutlet var textFieldCurrentPassword: UITextField!
    @IBOutlet var textFieldNewPassword: UITextField!
    @IBOutlet var textFieldConfirmNewPassword: UITextField!
    
    override func viewDidLoad() {
        
        textFieldsChangeListeners()
        setupDataBinding()
    }
    
    //MARK: User state
     override func viewWillAppear(_ animated: Bool) {
        if let user = Auth.auth().currentUser {
            user.reload(completion: { error in
                if error != nil {
                    self.performSegue(withIdentifier: CSegue.ChangePasswordToLogin, sender: nil)
                }
            })
        }
     }
    
    @IBAction func saveButtonClicked(_ sender: Any) {
        guard let currentPassword = textFieldCurrentPassword.text else {
            print("Current password is nil")
            return
        }
        guard let newPassword = textFieldNewPassword.text else {
            print("New password is nil")
            return
        }
        guard let confirmNewPassword = textFieldConfirmNewPassword.text else {
            print("Confirm new password is nil")
            return
        }
        changePasswordViewModel.changePassword(currentPassword: currentPassword, newPassword: newPassword, confirmNewPassword: confirmNewPassword)
    }
    
    
    //Disabling Save button when one TextField is empty
    private func textFieldsChangeListeners() {
        textFieldCurrentPassword.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        textFieldNewPassword.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        textFieldConfirmNewPassword.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
    }
    
    private func setupDataBinding() {
        changePasswordViewModel
        .changePassword
        .observeOn(MainScheduler.instance)
        .subscribe(onNext: { changePasswordEvent in
            switch changePasswordEvent {
            case .Success: self.showViewDismissDialogSuccess()
                case .Error(let error): do {
                        switch error {
                            case .NetworkError: self.dialogRepo.showNetworkErrorDialog(viewController: self)
                            case .InvalidEmail: self.dialogRepo.showInvalidEmailDialog(viewController: self)
                            case .UserNotFound: self.showNoUserFoundDialog()
                            case .PasswordsDoNotMatch: self.dialogRepo.showPasswordDoNotMatchDialog(viewController: self)
                            case .WeakPassword: self.dialogRepo.showWeakPasswordDialog(viewController: self)
                            case .WrongPassword: self.dialogRepo.showWrongPasswordDialog(viewController: self)
                            case .NewPasswordCanNotBeOld: self.dialogRepo.showNewPasswordCanNotBeOldDialog(viewController: self)
                            case .UnknownError: self.dialogRepo.showUnknownErrorDialog(viewController: self)
                    }
                }
            }
        })
        
        changePasswordViewModel
            .showLoading
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { loading in
                if loading {
                    self.buttonSave.isEnabled = false
                    super.showLoading(view: self.view)
                } else {
                    super.dismissLoading()
                }
            })
    }
    
    //MARK: Dialog
    private func showViewDismissDialogSuccess() {
        self.dialogRepo.showChangePasswordDialog(viewController: self, completion: {
            self.navigationController?.popViewController(animated: true)
        })
    }
    
    private func showNoUserFoundDialog() {
        self.dialogRepo.showNoUserFound(viewController: self, completion: {
            self.performSegue(withIdentifier: CSegue.ChangePasswordToLogin, sender: nil)
        })
    }
    
    @objc func textFieldDidChange(_ action: UIAlertAction) {
        if self.textFieldCurrentPassword.text == "" || self.textFieldNewPassword.text == "" || textFieldConfirmNewPassword.text == "" {
            self.buttonSave.isEnabled = false
        } else {
            self.buttonSave.isEnabled = true
        }
    }
}
