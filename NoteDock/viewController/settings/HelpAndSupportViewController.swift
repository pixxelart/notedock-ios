//
//  HelpAndSupportViewController.swift
//  NoteDock
//
//  Created by Branislav Bily on 01/01/2020.
//  Copyright © 2020 Pixel Art. All rights reserved.
//

import UIKit
import MessageUI
import FirebaseAuth

class HelpAndSupportViewController: UIViewController, MFMailComposeViewControllerDelegate {
 
    var helpAndSupportViewModel: HelpAndSupportViewViewModelProtocol!
    var cellRepo: CellRepository!
    
    @IBOutlet var tableView: UITableView!
    
    //MARK: User state
     override func viewWillAppear(_ animated: Bool) {
        if let user = Auth.auth().currentUser {
            user.reload(completion: { error in
                if error != nil {
                    self.performSegue(withIdentifier: CSegue.HelpViewToLogin, sender: nil)
                } else {
                    self.tableView.reloadData()
                }
            })
        }
     }
    
    //MARK: Email
    private func sendEmail(subject: String) {
        let recipientEmail = "branislav.bily@gmail.com"

        // Show default mail composer
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([recipientEmail])
            mail.setSubject(subject)
            present(mail, animated: true)

        // Show third party email composer if default Mail app is not present
        } else if let emailUrl = createEmailUrl(to: recipientEmail, subject: subject, body: "") {
            UIApplication.shared.open(emailUrl)
        } else {
            print("no email client found")
        }
    }
    
    private func createEmailUrl(to: String, subject: String, body: String) -> URL? {
        let subjectEncoded = subject.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!

        let gmailUrl = URL(string: "googlegmail://co?to=\(to)&subject=\(subjectEncoded)")
        let outlookUrl = URL(string: "ms-outlook://compose?to=\(to)&subject=\(subjectEncoded)")
        let yahooMail = URL(string: "ymail://mail/compose?to=\(to)&subject=\(subjectEncoded)")
        let sparkUrl = URL(string: "readdle-spark://compose?recipient=\(to)&subject=\(subjectEncoded)")
        let defaultUrl = URL(string: "mailto:\(to)?subject=\(subjectEncoded)")

        if let gmailUrl = gmailUrl, UIApplication.shared.canOpenURL(gmailUrl) {
            return gmailUrl
        } else if let outlookUrl = outlookUrl, UIApplication.shared.canOpenURL(outlookUrl) {
            return outlookUrl
        } else if let yahooMail = yahooMail, UIApplication.shared.canOpenURL(yahooMail) {
            return yahooMail
        } else if let sparkUrl = sparkUrl, UIApplication.shared.canOpenURL(sparkUrl) {
            return sparkUrl
        }
        return defaultUrl
    }

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}

extension HelpAndSupportViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.cellRepo.buildSettingsCell(indexPath: indexPath)
        if indexPath.section == 0 {
            cell.bindData(title: "Send bug report")
        } else {
            cell.bindData(title: "Send feedback")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            sendEmail(subject: "NoteDock: Bug Report")
        } else {
            sendEmail(subject: "NoteDock: Feedback")
        }
        tableView.deselectRow(at: indexPath, animated: false)
    }
}
