//
//  AccountViewController.swift
//  NoteDock
//
//  Created by Branislav Bily on 01/01/2020.
//  Copyright © 2020 Pixel Art. All rights reserved.
//

import UIKit
import RxSwift
import FirebaseAuth

class AccountViewController: LoadingSpinnerViewController {
    
    var accountViewModel: AccountViewViewModelProcotol!
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var imageProfile: CircularImageView!
    
    var handle: AuthStateDidChangeListenerHandle?
    var dialogRepo: DialogRepository!
    var cellRepo: CellRepository!
    
    var textFieldDisplayName: UITextField!
    var textFieldPassword: UITextField!
    
    var saveAction: UIAlertAction!
    var deleteAction: UIAlertAction!
    
    override func viewDidLoad() {
        setupDataBinding()
        
        imageProfile.image = UIImage(named: "logoGradient")
    }
    
    //MARK: UserState
    override func viewWillAppear(_ animated: Bool) {
        handle = Auth.auth().addStateDidChangeListener { (auth, user) in
            if user != nil {
                self.tableView.reloadData()
            } else {
                self.performSegue(withIdentifier: CSegue.AccountToLogin, sender: nil)
            }
        }
     }
        
    override func viewWillDisappear(_ animated: Bool) {
        Auth.auth().removeStateDidChangeListener(handle!)
    }
    
    //MARK: DataBinding
    private func setupDataBinding() {
        accountViewModel
            .accountDeleted
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { accountDeletedEvent in
                switch accountDeletedEvent {
                    case .Deleted: do {
                        //AuthStateListener automatically changes view to Login
                    }
                    case .Error(let error): do {
                        switch error {
                            case .NetworkError: self.dialogRepo.showNetworkErrorDialog(viewController: self)
                            case .UnknownError: self.dialogRepo.showUnknownErrorDialog(viewController: self)
                            case .NoUserFound: self.showNoUserFoundDialog()
                            case .WrongPassword: self.dialogRepo.showWrongPasswordDialog(viewController: self)
                            case .NoEmailFound: self.dialogRepo.showNoEmailFoundDialog(viewController: self)
                        }
                    }
                }
            })
        
        accountViewModel
            .showLoading
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { loading in
                if loading {
                    super.showLoading(view: self.view)
                } else {
                    super.dismissLoading()
                }
            })
        
        accountViewModel
            .updateDisplayName
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { updateEvent in
                switch updateEvent {
                    case .Success: self.tableView.reloadData()
                    case .Error: self.dialogRepo.showUnknownErrorDialog(viewController: self)
                }
            })
    }
    
    //MARK: CreateCell
    private func createEmailCell(cell: AccountDoubleDetailCellViewController) -> AccountDoubleDetailCellViewController {
        let title = "Email"
        if let email = Auth.auth().currentUser?.email {
            cell.bindData(title: title, detail: email)
        }
        cell.selectionStyle = .none
        return cell
    }
    
    private func createPhoneNumberCell(cell: AccountDoubleDetailCellViewController) -> AccountDoubleDetailCellViewController {
        let title = "Phone number"
        if let phoneNumber = Auth.auth().currentUser?.phoneNumber {
            cell.bindData(title: title, detail: phoneNumber)
        } else {
            cell.bindData(title: title, detail: "No phone number")
        }
        cell.selectionStyle = .none
        return cell
    }
    
    private func createAccountCreatedCell(cell: AccountDoubleDetailCellViewController) -> AccountDoubleDetailCellViewController {
        let title = "Member since"
        if let creationDate = Auth.auth().currentUser?.metadata.creationDate {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            let date = formatter.string(from: creationDate)
            cell.bindData(title: title, detail: date.description)
        }
        cell.selectionStyle = .none
        return cell
    }
    
    private func createDisplayNameCell(cell: AccountDoubleDetailCellViewController) -> AccountDoubleDetailCellViewController {
        let title = "Display Name"
        if let displayName = Auth.auth().currentUser?.displayName {
            cell.bindData(title: title, detail: displayName)
        } else {
            cell.bindData(title: title, detail: "No display name")
        }
        cell.selectionStyle = .none
        return cell
    }
    
    //MARK: Dialog
    private func deleteAccountDialog() {
        let alertController = UIAlertController(title: CDialogTitle.DeleteAccount,
                                                message: CDialogMessage.DeleteAccount,
                                                preferredStyle: .alert)
        alertController.addTextField { passwordTextField in
            passwordTextField.placeholder = CPlaceholder.Password
            self.textFieldPassword = passwordTextField
            self.textFieldPassword.isSecureTextEntry = true
        }

        let cancelAction = UIAlertAction(title: CAction.Cancel, style: .cancel)

        let deleteAction = UIAlertAction(title: CAction.Delete, style: .destructive) { action in
            let userPassword = self.textFieldPassword.text
            if let password = userPassword {
                self.accountViewModel.deleteAccount(password: password)
            }
        }
        //Disabling save action when PasswordTextField is empty
        self.deleteAction = deleteAction
        self.deleteAction.isEnabled = false
        textFieldPassword.addTarget(self, action: #selector(textFieldPasswordDidChange(_:)),
                                      for: UIControl.Event.editingChanged)

        alertController.addAction(cancelAction)
        alertController.addAction(self.deleteAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showChangeDisplayNameDialog() {
        let alertController = UIAlertController(title: CDialogTitle.DisplayName,
                                                message: CDialogMessage.DisplayName,
                                                preferredStyle: .alert)
        alertController.addTextField { (nameTextField) in
            if let displayName = Auth.auth().currentUser?.displayName {
                nameTextField.text = displayName
            } else {
                nameTextField.placeholder = CPlaceholder.DisplayName
            }
            self.textFieldDisplayName = nameTextField
        }
        let cancelAction = UIAlertAction(title: CAction.Cancel, style: .cancel)

        let saveAction = UIAlertAction(title: CAction.Save, style: .default) { action in
            self.accountViewModel.updateDisplayName(newName: self.textFieldDisplayName.text)
        }
        //Disabling save action when Folder name is empty
        self.saveAction = saveAction
        textFieldDisplayName.delegate = self
        self.saveAction.isEnabled = false
        textFieldDisplayName.addTarget(self, action: #selector(textFieldDisplayDidChange(_:)),
                                      for: UIControl.Event.editingChanged)
        textFieldDisplayName.addTarget(self, action: #selector(textField(_:shouldChangeCharactersIn:replacementString:)), for: .editingChanged)
        

        alertController.addAction(cancelAction)
        alertController.addAction(self.saveAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func showRemoveDialog(row: Int) {
        let alertController: UIAlertController
        if row == 1 {
            alertController = UIAlertController(title: CDialogTitle.RemovePhoneNumber, message: "", preferredStyle: .alert)
        } else {
            alertController = UIAlertController(title: CDialogTitle.RemoveDisplayName, message: "", preferredStyle: .alert)
        }
        let cancelAction = UIAlertAction(title: CAction.Cancel, style: .cancel)
        let removeAction = UIAlertAction(title: CAction.Delete, style: .destructive) { action in
            if row == 1 {
                print("Deleting phone number")
            } else {
                self.accountViewModel.updateDisplayName(newName: nil)
            }
        }
        alertController.addAction(cancelAction)
        alertController.addAction(removeAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func showNoUserFoundDialog() {
        self.dialogRepo.showNoUserFound(viewController: self, completion: {
            self.performSegue(withIdentifier: CSegue.AccountToLogin, sender: nil)
        })
    }
    
    //MARK: TextFieldChangeListener
    @objc func textFieldDisplayDidChange(_ action: UIAlertAction) {
        //If user has display name, do not allow him to change it to his current display name
        if let displayName = Auth.auth().currentUser?.displayName {
            if self.textFieldDisplayName.text == "" || self.textFieldDisplayName.text! == displayName {
                self.saveAction.isEnabled = false
            } else {
                self.saveAction.isEnabled = true
            }
        } else {
            if self.textFieldDisplayName.text == ""{
                self.saveAction.isEnabled = false
            } else {
                self.saveAction.isEnabled = true
            }
        }
    }
    
    @objc func textFieldPasswordDidChange(_ action: UIAlertAction) {
        if self.textFieldPassword.text == "" {
            self.deleteAction.isEnabled = false
        } else {
            self.deleteAction.isEnabled = true
        }
    }
}

extension AccountViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 20
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}

extension AccountViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.section == 0 {
            return true
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 3
        } else if section == 1 {
            return 1
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = self.cellRepo.buildAccountDoubleDetailCell(indexPath: indexPath)
            switch indexPath.row {
                case 0: do {
                    return createEmailCell(cell: cell)
                }
                case 1: do {
                    return createPhoneNumberCell(cell: cell)
                }
                default: do {
                    return createDisplayNameCell(cell: cell)
                }
            }
        } else if indexPath.section == 1 {
            let cell = self.cellRepo.buildAccountDoubleDetailCell(indexPath: indexPath)
            return createAccountCreatedCell(cell: cell)
        } else {
            let cell = self.cellRepo.buildAccountDeleteCell(indexPath: indexPath)
            cell.bindData(title: "Delete Account")
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 2 && indexPath.row == 0 {
            deleteAccountDialog()
            tableView.deselectRow(at: indexPath, animated: false)
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let remove = UITableViewRowAction(style: .destructive, title: CAction.Remove) { (action ,indexPath) in
            self.showRemoveDialog(row: indexPath.row)
        }
        let edit = UITableViewRowAction(style: .default, title: CAction.Change) { (action, indexPath) in
            if (indexPath.row == 0) {
                self.performSegue(withIdentifier: CSegue.AccountToUpdateEmail, sender: nil)
            } else if (indexPath.row == 1) {
                print("Phone number")
            } else {
                self.showChangeDisplayNameDialog()
            }
        }
        let add = UITableViewRowAction(style: .default, title: CAction.Add) { (action, indexPath) in
            if (indexPath.row == 0) {
                print("Email")
            } else if (indexPath.row == 1) {
                self.dialogRepo.showUpdatePhoneNumberDialog(viewController: self)
            } else {
                self.showChangeDisplayNameDialog()
            }
        }
        
        add.backgroundColor = UIColor.init(red: 0, green: 0.5, blue: 0, alpha: 1)
        edit.backgroundColor = UIColor.init(named: "NoteDockColor")
        if indexPath.section == 0 && indexPath.row == 1 {
            if (Auth.auth().currentUser?.phoneNumber) != nil {
                return [remove, edit]
            } else {
                return [add]
            }
        } else if indexPath.section == 0 && indexPath.row == 2 {
            if (Auth.auth().currentUser?.displayName) != nil {
                return [remove, edit]
            } else {
                return [add]
            }
        } else {
            return [edit]
        }
    }
}
