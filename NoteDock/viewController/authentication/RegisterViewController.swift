//
//  RegisterViewController.swift
//  NoteDock
//
//  Created by Branislav Bily on 29/12/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import UIKit
import RxSwift
import FirebaseAuth

class RegisterViewController: LoadingSpinnerViewController {
    
    @IBOutlet var textFieldEmail: UITextField!
    @IBOutlet var textFieldPassword: UITextField!
    @IBOutlet var textFieldConfirmPassword: UITextField!
    @IBOutlet var labelAlreadyHaveAccount: UILabel!
    
    var registerViewModel: RegisterViewViewModelProtocol!
    var dialogRepo: DialogRepository!
    
    @IBOutlet var buttonRegister: MainButton!
    
    override func viewDidLoad() {
        setupDataBinding()
        
        //Disabling Register button when one TextField is empty
        textFieldEmail.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        textFieldPassword.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        textFieldConfirmPassword.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        
        //Label tap recognizer
        let tap = UITapGestureRecognizer(target: self, action: #selector(labelClicked(sender:)))
        labelAlreadyHaveAccount.addGestureRecognizer(tap)
    }
    
    //MARK: Databinding
    func setupDataBinding() {
        registerViewModel
            .register
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { registerEvent in
                switch registerEvent {
                    case .Success: do {
                        self.dialogRepo.showRegisterSuccessDismissDialog(viewController: self)
                    }
                    case .Error(let registerError): do {
                        switch registerError {
                            case .NetworkError: self.dialogRepo.showNetworkErrorDialog(viewController: self)
                            case .InvadidEmail: self.dialogRepo.showInvalidEmailDialog(viewController: self)
                            case .WeakPassword: self.dialogRepo.showWeakPasswordDialog(viewController: self)
                            case .EmailAlreadyUsed: self.dialogRepo.showEmailAlreadyUsedDialog(viewController: self)
                            case .TooManyRequests: self.dialogRepo.showTooManyRequestsDialog(viewController: self)
                            case .PasswordDoNotMatch: self.dialogRepo.showPasswordDoNotMatchDialog(viewController: self)
                            case .UnknownError: self.dialogRepo.showUnknownErrorDialog(viewController: self)
                        }
                    }
                }
        })
        
        registerViewModel
            .showLoading
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { loading in
                if loading {
                    self.buttonRegister.isEnabled = false
                    super.showLoading(view: self.view)
                } else {
                    super.dismissLoading()
                }
            })
    }
    
    //MARK: Disabling TextField
    @objc func textFieldDidChange(_ action: UIAlertAction) {
        guard let email = textFieldEmail.text  else {
            print("Email is nil")
            return
        }
        guard let password = textFieldPassword.text else {
            print("Password is nil")
            return
        }
        guard let confirmPassword = textFieldConfirmPassword.text else {
            print("Confirm password is nil")
            return
        }
        if email == "" || password == "" || confirmPassword == "" {
            self.buttonRegister.isEnabled = false
        } else {
            self.buttonRegister.isEnabled = true
        }
    }
    
    //MARK: Button pressed
    @IBAction func registerButtonPressed(_ sender: Any) {
        let emailText = textFieldEmail.text
        let passwordText = textFieldPassword.text
        let confirmPasswordText = textFieldConfirmPassword.text
        
        guard let email = emailText else {
            print("Email is nil")
            return
        }
        guard let password = passwordText else {
            print("Password is nil")
            return
        }
        guard let confirmPassword = confirmPasswordText else {
            print("Confirm password is nil")
            return
        }
        view.endEditing(true)
        registerViewModel.register(email: email, password: password, confirmPassword: confirmPassword)
    }
    
    //MARK: Label click
    @objc func labelClicked(sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
}
