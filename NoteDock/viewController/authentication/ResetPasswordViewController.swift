//
//  ResetPasswordViewController.swift
//  NoteDock
//
//  Created by Branislav Bily on 30/12/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import UIKit
import RxSwift

class ResetPasswordViewController: LoadingSpinnerViewController {
    
    @IBOutlet var textFieldEmail: UITextField!
    @IBOutlet var buttonRecoverAccount: MainButton!
    
    var resetPasswordViewModel: ResetPasswordViewViewModelProtocol!
    var dialogRepo: DialogRepository!
    
    override func viewDidLoad() {
        
        setupDataBinding()
        //Disable Recover account button when textFieldEmail is empty
        textFieldEmail.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
    }
    
    //MARK: IBAction
    @IBAction func recoverAccountButtonPressed(_ sender: Any) {
        guard let email = textFieldEmail.text else {
            print("Email is nil")
            return
        }
        view.endEditing(true)
        resetPasswordViewModel.resetPassword(email: email)
    }
    
    //MARK: Databinding
    func setupDataBinding() {
        resetPasswordViewModel
            .resetPassword
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { resetPasswordEvent in
                switch resetPasswordEvent {
                case .Success: self.dialogRepo.showVerificationEmailDismissSent(viewController: self)
                    case .Error(let resetPasswordError): do {
                        switch resetPasswordError {
                            case .InvalidEmail: self.dialogRepo.showInvalidEmailDialog(viewController: self)
                            case .NetworkError: self.dialogRepo.showNetworkErrorDialog(viewController: self)
                            case .NoAccount: self.dialogRepo.showNoAccountErrorDialog(viewController: self)
                            case .UnknownError: self.dialogRepo.showUnknownErrorDialog(viewController: self)
                        }
                    }
                }
        })
        
        resetPasswordViewModel
            .showLoading
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { loading in
                if loading {
                    self.buttonRecoverAccount.isEnabled = false
                    super.showLoading(view: self.view)
                } else {
                    self.buttonRecoverAccount.isEnabled = true
                    super.dismissLoading()
                }
            })
    }
    
    //MARK: TextFieldChangeListener
    @objc func textFieldDidChange(_ action: UIAlertAction) {
        if self.textFieldEmail.text == "" {
            self.buttonRecoverAccount.isEnabled = false
        } else {
            self.buttonRecoverAccount.isEnabled = true
        }
    }
}
