//
//  CircularImageView.swift
//  NoteDock
//
//  Created by Branislav Bily on 20/01/2020.
//  Copyright © 2020 Pixel Art. All rights reserved.
//

import UIKit

class CircularImageView: UIImageView {
    
    private func roundUpImage() {
        self.layer.masksToBounds = false
        //If image height is uneven number
        self.layer.cornerRadius = (self.frame.height / 2).rounded(.up)
        self.clipsToBounds = true
    }
    
    override var image: UIImage? {
        didSet {
            roundUpImage()
        }
    }
}
