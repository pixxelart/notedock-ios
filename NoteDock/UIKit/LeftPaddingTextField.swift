//
//  LeftPaddingTextField.swift
//  NoteDock
//
//  Created by Branislav Bily on 29/12/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import UIKit

class LeftPaddingTextField: UITextField {
    
    let padding = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 5)

    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
