//
//  NoteCellViewController.swift
//  NoteDock
//
//  Created by Branislav Bily on 01/12/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import UIKit

class NoteCellViewController: UITableViewCell {
    
    @IBOutlet var noteTitle: UILabel!
    @IBOutlet var noteDescription: UILabel!
    @IBOutlet weak var noteUpdated: UILabel!
    
    func bindData(noteModel: NoteModel) {
        if let title = noteModel.title {
            noteTitle.text = title
        }
        
        if let description = noteModel.description {
            noteDescription.text = description
        }
        
        if let updated = noteModel.updated {
            noteUpdated.text = updated
        }
    }
}



