//
//  folderCellViewController.swift
//  NoteDock
//
//  Created by Branislav Bily on 30/09/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//
import UIKit

class FolderCellViewController: UITableViewCell {
    
    @IBOutlet var labelName: UILabel!
    @IBOutlet var labelNotesCount: UILabel!
    
    func bindData(folder: FolderModel) {
        if let name = folder.name {
            self.labelName.text = name
        }
        if let notesCount = folder.notesCount {
            self.labelNotesCount.text = String(notesCount)
        }
    }
}
