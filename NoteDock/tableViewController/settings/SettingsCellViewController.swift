//
//  SettingsCellViewController.swift
//  NoteDock
//
//  Created by Branislav Bily on 31/12/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import UIKit

class SettingsCellViewController: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    @IBOutlet var labelTitle: UILabel!
    
    func bindData(title: String) {
        labelTitle.text = title
    }
}
