//
//  AccountDetailViewController.swift
//  NoteDock
//
//  Created by Branislav Bily on 20/01/2020.
//  Copyright © 2020 Pixel Art. All rights reserved.
//

import UIKit
import FirebaseAuth

class AccountDetailCellViewController: UITableViewCell {
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageProfile: CircularImageView!
    
    func bindData(profileImage: UIImage?) {
        if let user = Auth.auth().currentUser {
            if let displayName = user.displayName {
                self.labelTitle.text = displayName
            } else if let email = user.email {
                self.labelTitle.text = email
            }
        }
        
        if let image = profileImage {
            self.imageProfile.image = image
        }
    }
}
