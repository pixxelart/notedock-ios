//
//  AccountDoubleDetailCellViewController.swift
//  NoteDock
//
//  Created by Branislav Bily on 01/01/2020.
//  Copyright © 2020 Pixel Art. All rights reserved.
//

import UIKit

class AccountDoubleDetailCellViewController: UITableViewCell {
    
    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var labelDetail: UILabel!
    
    func bindData(title: String, detail: String) {
        labelTitle.text = title
        labelDetail.text = detail
    }
}
