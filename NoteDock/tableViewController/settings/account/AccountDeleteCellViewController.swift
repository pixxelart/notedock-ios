//
//  AccountDeleteCellViewController.swift
//  NoteDock
//
//  Created by Branislav Bily on 01/01/2020.
//  Copyright © 2020 Pixel Art. All rights reserved.
//

import UIKit

class AccountDeleteCellViewController: UITableViewCell {
    
    @IBOutlet var labelTitle: UILabel!
    
    func bindData(title: String) {
        labelTitle.text = title
    }
}
