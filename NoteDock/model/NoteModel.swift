//
//  NoteModel.swift
//  NoteDock
//
//  Created by Branislav Bily on 01/12/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import Foundation
import FirebaseFirestore

class NoteModel {
    
    var uid: String? = ""
    var title: String? = ""
    var description: String? = ""
    var updated: String? = ""
    
    init(uid: String, title: String, description: String) {
        self.uid = uid
        self.title = title
        self.description = description
    }
    
    init(document: QueryDocumentSnapshot) {
        self.uid = document.documentID
        
        if let title = document.get(CNote.Title) as? String {
            self.title = title
        }
        
        if let description = document.get(CNote.Description) as? String {
            self.description = description
        }
        
        if let updatedTimestamp = document.get(CNote.Updated) as? Timestamp {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            let date = formatter.string(from: updatedTimestamp.dateValue())
            self.updated = date
        }
    }
    
    init(document: DocumentSnapshot) {
        self.uid = document.documentID
        
        if let title = document.get(CNote.Title) as? String {
            self.title = title
        }
        
        if let description = document.get(CNote.Description) as? String {
            self.description = description
        }
    }
    
}
