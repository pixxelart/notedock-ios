//
//  Folder.swift
//  NoteDock
//
//  Created by Branislav Bily on 30/09/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import Foundation
import FirebaseFirestore

class FolderModel {
    
    var uid: String? = ""
    var name: String? = ""
    var notesCount: Int? = 0
    
    init(folderName: String, notesCount: Int) {
        self.name = folderName
        self.notesCount = notesCount
    }
    
    init(document: QueryDocumentSnapshot) {
        self.uid = document.documentID
        
        if let name = document.get(CFolder.Name) as? String {
            self.name = name
        }
        if let notesCount = document.get(CFolder.NotesCount) as? Int {
            self.notesCount = notesCount
        }
    }
}
