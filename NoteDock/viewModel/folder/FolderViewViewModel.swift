//
//  FolderViewViewModel.swift
//  NoteDock
//
//  Created by Branislav Bily on 25/12/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import RxSwift
import FirebaseAuth
import Crashlytics

protocol FolderViewViewModelProtocol {
    var notesLoaded: PublishSubject<LoadNotesEvent> {get}
    var noteAdded: PublishSubject<AddNoteEvent> {get}
    var noteDeleted: PublishSubject<DeleteNoteEvent> {get}
    func loadNotes(folderUUID: String)
    func addNote(folderUUID: String)
    func deleteNote(folderUUID: String, noteUUID: String)
}

class FolderViewViewModel: FolderViewViewModelProtocol {
    var noteAdded: PublishSubject<AddNoteEvent> = PublishSubject()
    var notesLoaded: PublishSubject<LoadNotesEvent> = PublishSubject()
    var noteDeleted: PublishSubject<DeleteNoteEvent> = PublishSubject()
    
    var deleteNoteFromFirebase: DeleteNoteFromFirebaseUseCase!
    var loadNotesFromFirebase: LoadNotesFromFirebaseUseCase!
    var addNoteToFirebase: AddNoteToFirebaseUseCase!
    
    func loadNotes(folderUUID: String) {
        if let user = Auth.auth().currentUser {
            self.loadNotesFromFirebase
            .loadNotes(folderUUID: folderUUID, user: user)
            .asObservable()
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribe(onNext: { notes in
                self.notesLoaded.onNext(.Success(notes))
            }, onError: { error in
                Crashlytics.sharedInstance().recordError(error)
                self.notesLoaded.onNext(.Error(.UnknownError))
            })
        } else {
            self.notesLoaded.onNext(.Error(.UnknownError))
        }
    }
    
    func deleteNote(folderUUID: String, noteUUID: String) {
        if let user = Auth.auth().currentUser {
            deleteNoteFromFirebase
                .deleteNote(folderUUID: folderUUID, noteUUID: noteUUID, user: user)
                .asObservable()
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .subscribe(onError: { error in
                    Crashlytics.sharedInstance().recordError(error)
                    self.noteDeleted.onNext(.Error(.UnknownError))
                }, onCompleted: {
                    self.noteDeleted.onNext(.Success)
                })
        } else {
            self.noteDeleted.onNext(.Error(.UnknownError))
        }
    }
    
    
    func addNote(folderUUID: String) {
        if let user = Auth.auth().currentUser {
            self.addNoteToFirebase
                .addNote(folderUUID: folderUUID, user: user)
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .subscribe(onSuccess: { noteUUID in
                    self.noteAdded.onNext(.Success(noteUUID))
                }, onError: { error in
                    Crashlytics.sharedInstance().recordError(error)
                    self.noteAdded.onNext(.Error(.UnknownError))
                })
        } else {
            self.noteAdded.onNext(.Error(.UnknownError))
        }
    }
}

enum LoadNotesEvent {
    case Success([NoteModel])
    case Error(LoadNotesEventError)
}

enum LoadNotesEventError {
    case UnknownError
}

enum AddNoteEvent {
    case Success(String)
    case Error(AddNoteEventError)
}

enum AddNoteEventError {
    case UnknownError
}
