//
//  foldersViewViewModel.swift
//  NoteDock
//
//  Created by Branislav Bily on 16/12/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.

import RxSwift
import FirebaseAuth
import Crashlytics

protocol FoldersViewViewModelProtocol {
    var loadFolders: PublishSubject<LoadFoldersEvent> {get}
    var createFolder: PublishSubject<AddFolderEvent> {get}
    var deleteFolder: PublishSubject<DeleteFolderEvent> {get}
    var showLoading: PublishSubject<Bool> {get}
    func loadFoldersFromFirebase()
    func createFolder(folderName: String)
    func deleteFolder(folderUUID: String)
}

class FoldersViewViewModel: FoldersViewViewModelProtocol {
    public var loadFolders: PublishSubject<LoadFoldersEvent> = PublishSubject()
    public var createFolder: PublishSubject<AddFolderEvent> = PublishSubject()
    public var deleteFolder: PublishSubject<DeleteFolderEvent> = PublishSubject()
    public var showLoading: PublishSubject<Bool> = PublishSubject()
    
    var foldersFromFirebase: FoldersFromFirebaseUseCase!
    var addFolderToFirebase: AddFolderToFirebaseUseCase!
    var folderNameTaken: FolderNameTakenUseCase!
    var deleteFolderFromFirebase: DeleteFolderFromFirebaseUseCase!
    
    public func loadFoldersFromFirebase() {
        if let user = Auth.auth().currentUser {
            self.foldersFromFirebase
                .loadFolders(user: user)
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .subscribe(onNext: { folders in
                    self.showLoading.onNext(false)
                    self.loadFolders.onNext(.Success(folders))
                }, onError: { (error) in
                    self.showLoading.onNext(false)
                    Crashlytics.sharedInstance().recordError(error)
                    self.loadFolders.onNext(.Error(.UnknownError))
                })
        } else {
            self.loadFolders.onNext(.Error(.NoUserFound))
        }
    }
    
    public func createFolder(folderName: String) {
        //First check if folderName taken
        if let user = Auth.auth().currentUser {
            self.folderNameTaken
                .isNameTaken(folderName: folderName,user: user)
                .asObservable()
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .subscribe(onNext: { nameTaken in
                    if(!nameTaken) {
                        self.addFolder(folderName: folderName)
                    } else {
                        self.createFolder.onNext(.Error(.NameTaken))
                    }
                }, onError: { error in
                    Crashlytics.sharedInstance().recordError(error)
                    self.createFolder.onNext(.Error(.UnknownError))
                })
        } else {
            self.createFolder.onNext(.Error(.NoUserFound))
        }
    }
    
    private func addFolder(folderName: String) {
        if let user = Auth.auth().currentUser {
            self.addFolderToFirebase
                .addFolder(folderName: folderName, user: user)
                .asObservable()
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .subscribe(onNext: { documentID in
                    self.createFolder.onNext(.Added(documentID))
                }, onError: { error in
                    Crashlytics.sharedInstance().recordError(error)
                    self.createFolder.onNext(.Error(.UnknownError))
                })
        } else {
            self.createFolder.onNext(.Error(.NoUserFound))
        }
    }
    
    public func deleteFolder(folderUUID: String) {
        if let user = Auth.auth().currentUser {
            self.deleteFolderFromFirebase
            .deleteFolder(folderUUID: folderUUID, user: user)
            .asObservable()
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribe(onNext: { documentID in
                self.deleteFolder.onNext(.Success)
            }, onError: { error in
                Crashlytics.sharedInstance().recordError(error)
                self.deleteFolder.onNext(.Error(.UnknownError))
            })
        } else {
            self.deleteFolder.onNext(.Error(.NoUserFound))
        }
    }
}

enum LoadFoldersEvent {
    case Success([FolderModel])
    case Error(LoadFoldersEventError)
}

enum LoadFoldersEventError {
    case UnknownError
    case NoUserFound
}

enum AddFolderEvent {
    case Added(String)
    case Error(AddFolderEventError)
}

enum AddFolderEventError {
    case NameTaken
    case NoUserFound
    case UnknownError
}


enum DeleteFolderEvent {
    case Success
    case Error(DeleteFolderEventError)
}

enum DeleteFolderEventError {
    case UnknownError
    case NoUserFound
}
