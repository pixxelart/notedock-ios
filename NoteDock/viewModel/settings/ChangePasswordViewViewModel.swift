//
//  ChangePasswordViewViewModel.swift
//  NoteDock
//
//  Created by Branislav Bily on 01/01/2020.
//  Copyright © 2020 Pixel Art. All rights reserved.
//

import RxSwift
import FirebaseAuth
import Crashlytics

protocol ChangePasswordViewViewModelProtocol {
    var changePassword: PublishSubject<ChangePasswordEvent> {get}
    func changePassword(currentPassword: String, newPassword: String, confirmNewPassword: String)
    var showLoading: PublishSubject<Bool> {get}
}

class ChangePasswordViewViewModel: ChangePasswordViewViewModelProtocol {
    var changePassword: PublishSubject<ChangePasswordEvent> = PublishSubject()
    var showLoading: PublishSubject<Bool> = PublishSubject()
    
    var authRepository: AuthRepository!
    
    func changePassword(currentPassword: String, newPassword: String, confirmNewPassword: String) {
        if !newPassword.elementsEqual(confirmNewPassword){
            changePassword.onNext(.Error(.PasswordsDoNotMatch))
        } else if newPassword.elementsEqual(currentPassword) {
            changePassword.onNext(.Error(.NewPasswordCanNotBeOld))
        } else {
            guard let user = Auth.auth().currentUser else {
                changePassword.onNext(.Error(.UserNotFound))
                return
            }
            guard let email = user.email else {
                changePassword.onNext(.Error(.InvalidEmail))
                return
            }
            self.checkIfCorrectPassword(user: user, email: email, currentPassword: currentPassword, newPassword: newPassword)
        }
    }
    
    private func checkIfCorrectPassword(user: User, email: String, currentPassword: String, newPassword: String) {
        self.showLoading.onNext(true)
        authRepository
            .reauthenticateUser(user: user, email: email, password: currentPassword)
            .asObservable()
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribe(onNext: { correctPassword in
                self.changeCurrentPasswordToNew(user: user, newPassword: newPassword)
            }, onError: { error in
                self.showLoading.onNext(false)
                self.changePassword.onNext(.Error(self.handleError(error: error as NSError)))
            })
    }
    
    private func changeCurrentPasswordToNew(user: User, newPassword: String) {
        authRepository
            .changePassword(user: user, newPassword: newPassword)
            .asObservable()
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribe(onNext: { changed in
                self.showLoading.onNext(false)
                self.changePassword.onNext(.Success)
            }, onError: { error in
                self.showLoading.onNext(false)
                self.changePassword.onNext(.Error(self.handleError(error: error as NSError)))
            })
    }
    
    func handleError(error: NSError) -> ChangePasswordError {
        switch error.code {
            case AuthErrorCode.networkError.rawValue: return .NetworkError
            case AuthErrorCode.weakPassword.rawValue: return .WeakPassword
            case AuthErrorCode.wrongPassword.rawValue: return .WrongPassword
            default:
                Crashlytics.sharedInstance().recordError(error)
                return .UnknownError
            }
    }
}

enum ChangePasswordEvent {
    case Success
    case Error(ChangePasswordError)
}

enum ChangePasswordError {
    case NetworkError
    case UserNotFound
    case PasswordsDoNotMatch
    case WeakPassword
    case UnknownError
    case InvalidEmail
    case WrongPassword
    case NewPasswordCanNotBeOld
}
