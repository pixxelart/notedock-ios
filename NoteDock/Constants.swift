//
//  Constants.swift
//  NoteDock
//
//  Created by Branislav Bily on 30/09/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import Foundation
import FirebaseFirestore

let firestoreReference = Firestore.firestore()

struct CCollections {
    static let Folders = "folders"
    static let Notes = "notes"
    static let Users = "users"
}

struct CFolder {
    static let Name = "name"
    static let NotesCount = "notesCount"
    static let Added = "added"
}

struct CNote {
    static let Title = "title"
    static let Description = "description"
    static let Updated = "updated"
}

struct CDialogTitle {
    static let Error = "An error occured!"
    static let Success = "Success!"
    static let DeleteFolder = "Delete folder?"
    static let DeleteNote = "Delete note?"
    static let NameTaken = "Name is already taken!"
    static let RateUs = "Can not perform this action."
    static let DeleteAccount = "Delete account?"
    static let EmailNotVerified = "Your email is not verified!"
    static let EmailVerificationSent = "Verification email sent!"
    static let UseNewEmail = "Please choose new email"
    static let EmailAlreadyUsed = "Email already used"
    static let EmailChanged = "Email changed"
    static let DisplayName = "Display Name"
    static let RemovePhoneNumber = "Remove phone number?"
    static let RemoveDisplayName = "Remove display name?"
    static let UpdatePhoneNumber = "This feature is not available yet!"
    static let AccountCreated = "Your account has been created!"
    static let CreateFolder = "Create folder"
}

struct CDialogMessage {
    static let CreateFolder = "Enter folder name"
    static let NetworkError = "Please check your internet connection"
    static let EmailAlreadyUsed = "Please choose another email"
    static let InvalidPassword = "You entered wrong password!"
    static let InvalidEmail = "Invalid email address!"
    static let TryAgain = "Please try again!"
    static let EmailChanged = "Please validate your new email!"
    static let PasswordsDoNotMatch = "Please make sure the passwords match"
    static let WeakPassword = "Please choose a stronger password!"
    static let WrongPassword = "You entered wrong password!"
    static let NewPasswordCanNotBeOld = "You can not change to current password!"
    static let NoEmailFound = "No email found!"
    static let DeleteAccount = "Enter password if you really want to delete you account"
    static let DisplayName = "Enter new display name"
    static let UpdatePhoneNumber = "Make sure to update the app to not miss out!"
    static let RateUs = "We are not on App Store yet."
    static let BadCredentials = "Email or password are wrong!"
    static let TooManyRequests = "You are sending too many requests. Please try again"
    static let EmailNotVerified = "Do you wish to send activation email?"
    static let NoAccount = "No account with this email found!"
    static let EmailSent = "Check your email!"
    static let PasswordChanged = "Your password has been changed!"
    static let DeleteFolder = "You will delete folder with all its notes!"
    static let DeleteNote = "You can not go back!"
    static let NameTaken = "Please choose another name"
    static let UserNotFound = "Please login again"
}

struct CAction {
    static let Ok = "Ok"
    static let Cancel = "Cancel"
    static let Save = "Save"
    static let Delete = "Delete"
    static let Remove = "Remove"
    static let Change = "Change"
    static let Add = "Add"
    static let Send = "Send"
    static let Edit = "Edit"
}

struct CPlaceholder {
    static let Password = "Password"
    static let NoteBook = "Notebook"
    static let DisplayName = "Display Name"
}

struct CSegue {
    static let FoldersToLogin = "foldersToLoginSeg"
    static let FoldersToSettings = "foldersToSettingsSeg"
    static let FoldersToFolder = "foldersToFolderSeg"
    static let FoldersToEdit = "foldersToEditSeg"
    static let FolderToLogin = "folderViewToLoginSeg"
    static let FolderToNote = "folderToNoteSeg"
    static let EditToLogin = "editViewToLoginSeg"
    static let LoginToNavigation = "loginToNaviSeg"
    static let LoginToRegister = "loginToRegSeg"
    static let LoginToResetSeg = "loginToResetSeg"
    static let SettingsToLogin = "settingsViewToLoginSeg"
    static let SettingsToAccount = "settingsToAccountSeg"
    static let SettingsToChangePassword = "settingsToChangePasswordSeg"
    static let SettingsToHelp = "settingsToHelpSeg"
    static let AccountToLogin = "accountViewToLoginSeg"
    static let AccountToUpdateEmail = "accountToUpdateEmail"
    static let ChangePasswordToLogin = "changePasswordViewToLoginSeg"
    static let HelpViewToLogin = "helpViewToLoginSeg"
    static let UpdateEmailToLogin = "updateEmailViewToLoginSeg"
    static let NoteToLogin = "noteViewToLoginSeg"
    static let NoteToFolder = "noteToFolderSeg"
}
