//
//  DeleteFolderFromFirebaseUseCase.swift
//  NoteDock
//
//  Created by Branislav Bily on 28/12/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import RxSwift
import FirebaseAuth
import RxFirebaseFirestore

protocol DeleteFolderFromFirebaseUseCase {
    func deleteFolder(folderUUID: String, user: User) -> Completable
}

class DeleteFolderFromFirebaseImpl: DeleteFolderFromFirebaseUseCase {
    func deleteFolder(folderUUID: String, user: User) -> Completable {
        return firestoreReference.collection(CCollections.Users)
            .document(user.uid)
            .collection(CCollections.Folders)
            .document(folderUUID)
            .rx
            .delete()
            .asSingle()
            .asCompletable()
    }
}
