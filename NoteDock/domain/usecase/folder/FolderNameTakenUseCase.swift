//
//  FolderNameTakenUseCase.swift
//  NoteDock
//
//  Created by Branislav Bily on 25/12/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import RxSwift
import RxFirebaseFirestore
import FirebaseAuth

protocol FolderNameTakenUseCase {
    func isNameTaken(folderName: String, user: User) -> Single<Bool>
}

class FolderNameTakenImpl: FolderNameTakenUseCase {
    func isNameTaken(folderName: String, user: User) -> Single<Bool> {
        return firestoreReference.collection(CCollections.Users)
                .document(user.uid)
                .collection(CCollections.Folders)
                .whereField(CFolder.Name, isEqualTo: folderName)
                .rx
                .getDocuments()
                .map { querySnapshot -> Bool in !querySnapshot.isEmpty}
                .asSingle()
    }
}
