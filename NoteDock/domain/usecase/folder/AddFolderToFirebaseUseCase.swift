//
//  AddFolderToFirebaseUseCase.swift
//  NoteDock
//
//  Created by Branislav Bily on 30/09/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import RxSwift
import RxFirebaseFirestore
import FirebaseFirestore
import FirebaseAuth

protocol AddFolderToFirebaseUseCase {
    func addFolder(folderName: String, user: User) -> Single<String>
}

class AddFolderToFirebaseImpl: AddFolderToFirebaseUseCase {
    func addFolder(folderName: String, user: User) -> Single<String> {
           return firestoreReference.collection(CCollections.Users)
                    .document(user.uid)
                    .collection(CCollections.Folders)
                    .rx
                    .addDocument(data: [
                        CFolder.Name: folderName,
                        CFolder.NotesCount: 0,
                        CFolder.Added: Timestamp(date: Date())
                    ])
                    .map { document -> String in document.documentID}
                    .asSingle()
    }
}
