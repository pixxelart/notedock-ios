//
//  FoldersFromFirebaseUseCase.swift
//  NoteDock
//
//  Created by Branislav Bily on 30/09/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//
import RxSwift
import RxFirebaseFirestore
import FirebaseAuth

protocol FoldersFromFirebaseUseCase {
    func loadFolders(user: User) -> Observable<[FolderModel]>
}

class FoldersFromFirebaseImpl: FoldersFromFirebaseUseCase {
    func loadFolders(user: User) -> Observable<[FolderModel]> {
        return firestoreReference.collection(CCollections.Users)
            .document(user.uid)
            .collection(CCollections.Folders)
            .order(by: CFolder.Added, descending: true)
            .rx
            .listen()
            .map { snapshot -> [FolderModel] in
                let documents = snapshot.documents
                var folders: [FolderModel] = []
                for document in documents {
                    folders.append(FolderModel(document: document))
                }
                return folders
        }
    }
}
