//
//  RenameFolderUseCase.swift
//  NoteDock
//
//  Created by Branislav Bily on 28/12/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import RxSwift
import FirebaseAuth
import RxFirebaseFirestore

protocol RenameFolderUsecase {
    func renameFolder(folderUUID: String, newName: String, user: User) -> Completable
}

class RenameFolderImpl: RenameFolderUsecase {
    func renameFolder(folderUUID: String, newName: String, user: User) -> Completable {
        return firestoreReference.collection(CCollections.Users)
                .document(user.uid)
                .collection(CCollections.Folders)
                .document(folderUUID)
                .rx
                .updateData([CFolder.Name: newName])
                .asSingle()
                .asCompletable()
    }
}
