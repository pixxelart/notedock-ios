//
//  AddNoteToFirebaseUseCase.swift
//  NoteDock
//
//  Created by Branislav Bily on 28/12/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import RxSwift
import FirebaseAuth
import FirebaseFirestore

protocol AddNoteToFirebaseUseCase {
    func addNote(folderUUID: String, user: User) -> Single<String>
}

class AddNoteToFirebaseImpl: AddNoteToFirebaseUseCase {
    func addNote(folderUUID: String, user: User) -> Single<String> {
            return firestoreReference.collection(CCollections.Users)
                .document(user.uid)
                .collection(CCollections.Folders)
                .document(folderUUID)
                .collection(CCollections.Notes)
                .rx
                .addDocument(data:
                    [CNote.Title: "Untitled",
                     CNote.Updated: Timestamp(date: Date())
                ])
                .map { document -> String in document.documentID }
                .asSingle()
    }
}
