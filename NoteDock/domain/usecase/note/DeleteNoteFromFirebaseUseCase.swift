//
//  DeleteNoteFromFirebaseUseCase.swift
//  NoteDock
//
//  Created by Branislav Bily on 28/12/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import RxSwift
import FirebaseAuth
import RxFirebaseFirestore

protocol DeleteNoteFromFirebaseUseCase {
    func deleteNote(folderUUID: String, noteUUID: String, user: User) -> Completable
}

class DeleteNoteFromFirebaseImpl: DeleteNoteFromFirebaseUseCase {
    func deleteNote(folderUUID: String, noteUUID: String, user: User) -> Completable {
        return firestoreReference.collection(CCollections.Users)
                .document(user.uid)
                .collection(CCollections.Folders)
                .document(folderUUID)
                .collection(CCollections.Notes)
                .document(noteUUID)
                .rx
                .delete()
                .asSingle()
                .asCompletable()
    }
}
