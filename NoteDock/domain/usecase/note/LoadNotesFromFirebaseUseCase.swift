//
//  NotesFromFirebaseUseCase.swift
//  NoteDock
//
//  Created by Branislav Bily on 01/12/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import RxSwift
import FirebaseAuth
import RxFirebaseFirestore

protocol LoadNotesFromFirebaseUseCase {
    func loadNotes(folderUUID: String, user: User) -> Observable<[NoteModel]>
}

class LoadNotesFromFirebaseImpl: LoadNotesFromFirebaseUseCase {
    
func loadNotes(folderUUID: String, user: User) -> Observable<[NoteModel]> {
    return firestoreReference.collection(CCollections.Users)
        .document(user.uid)
        .collection(CCollections.Folders)
        .document(folderUUID)
        .collection(CCollections.Notes)
        .order(by: CNote.Updated, descending: true)
        .rx
        .listen()
        .map { snapshot -> [NoteModel] in
            let documents = snapshot.documents
            var notes: [NoteModel] = []
            for document in documents {
                notes.append(NoteModel(document: document))
            }
            return notes
        }
    }
}
