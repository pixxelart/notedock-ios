//
//  DITesting.swift
//  NoteDock
//
//  Created by Branislav Bily on 13/09/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import Foundation

protocol DITestingUseCase {
    func testing() -> String
}

class DITestingImpl: DITestingUseCase {
    func testing() -> String {
        return "Working DI"
    }
}
