//
//  authRepository.swift
//  NoteDock
//
//  Created by Branislav Bily on 30/12/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import RxSwift
import FirebaseAuth
import RxFirebaseAuthentication

protocol AuthRepository {
    func login(email: String, password: String) -> Completable
    func register(email: String, password: String) -> Completable
    func sendPasswordResetEmail(email: String) -> Completable
    func sendVerificationEmail(user: User) -> Single<Bool>
    func deleteUser(user: User) -> Single<Bool>
    func reauthenticateUser(user: User, email: String, password: String) -> Single<Bool>
    func changePassword(user: User, newPassword: String) -> Single<Bool>
    func updateEmail(user: User, email: String) -> Single<Bool>
}

class AuthRepositoryImpl: AuthRepository {
    func deleteUser(user: User) -> Single<Bool> {
        return Single.create { emitter in
            user.delete() { error in
                if let error = error {
                    emitter(.error(error))
                } else {
                    emitter(.success(true))
                }
            }
            return Disposables.create {/** Nothing to cancel **/ }
        }
    }
    
    func changePassword(user: User, newPassword: String) -> Single<Bool> {
        return Single.create { emitter in
            user.updatePassword(to: newPassword) { error in
                if let error = error {
                    emitter(.error(error))
                } else {
                    emitter(.success(true))
                }
            }
            return Disposables.create {/** Nothing to cancel **/ }
        }
    }
    
    func reauthenticateUser(user: User, email: String, password: String) -> Single<Bool> {
        return Single.create { emitter in
            let credential = EmailAuthProvider.credential(withEmail: email, password: password)
            user.reauthenticate(with: credential) { (result, error) in
                if let error = error {
                    emitter(.error(error))
                } else {
                    emitter(.success(true))
                }
            }
            return Disposables.create { /** Nothing to cancel **/ }
        }
    }
    
    func login(email: String, password: String) -> Completable {
            return Auth.auth()
                .rx
                .signIn(withEmail: email, password: password)
                .asSingle()
                .asCompletable()
}
    
    func register(email: String, password: String) -> Completable {
        return Auth.auth()
                .rx
                .createUser(withEmail: email, password: password)
                .asSingle()
                .asCompletable()
    }
    
    func sendPasswordResetEmail(email: String) -> Completable {
        return Auth.auth()
                .rx
                .sendPasswordReset(withEmail: email)
                .asSingle()
                .asCompletable()
    }
    
    func sendVerificationEmail(user: User) -> Single<Bool> {
        return Single.create { emitter in
            user.sendEmailVerification { authError in
                if let error = authError {
                    emitter(.error(error))
                } else {
                    emitter(.success(true))
                }
            }
            return Disposables.create { }
        }
    }
    
    func updateEmail(user: User, email: String) -> Single<Bool> {
        return Single.create { emitter in
            user.updateEmail(to: email) { error in
                if let error = error {
                    emitter(.error(error))
                } else {
                    emitter(.success(true))
                }
            }
            return Disposables.create { }
        }
    }
}
