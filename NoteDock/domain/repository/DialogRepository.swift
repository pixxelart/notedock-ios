//
//  DialogRepository.swift
//  NoteDock
//
//  Created by Branislav Bily on 25/01/2020.
//  Copyright © 2020 Pixel Art. All rights reserved.
//

import UIKit

protocol DialogRepository {
    func showDeleteFolderDialog(viewController: UIViewController, completion: @escaping() -> Void)
    func showDeleteNoteDialog(viewController: UIViewController, completion: @escaping() -> Void)
    func showUserNotVerifiedDialog(viewController: UIViewController, completion: @escaping() -> Void)
    func showEmailChangedDialog(viewController: UIViewController, completion: @escaping() -> Void)
    func showChangePasswordDialog(viewController: UIViewController, completion: @escaping() -> Void)
    func showNoUserFound(viewController: UIViewController, completion: @escaping() -> Void)
    func showInvalidEmailDialog(viewController: UIViewController)
    func showInvalidEmailDismissDialog(viewController: UIViewController)
    func showNetworkErrorDialog(viewController: UIViewController)
    func showNoAccountErrorDialog(viewController: UIViewController)
    func showNameTakenDialog(viewController: UIViewController)
    func showWeakPasswordDialog(viewController: UIViewController)
    func showEmailAlreadyUsedDialog(viewController: UIViewController)
    func showTooManyRequestsDialog(viewController: UIViewController)
    func showNoEmailFoundDialog(viewController: UIViewController)
    func showPasswordDoNotMatchDialog(viewController: UIViewController)
    func showUseNewEmailDialog(viewController: UIViewController)
    func showTryAgainDialog(viewController: UIViewController)
    func showBadCredentialsDialog(viewController: UIViewController)
    func showNewPasswordCanNotBeOldDialog(viewController: UIViewController)
    func showInvalidPasswordDialog(viewController: UIViewController)
    func showUpdatePhoneNumberDialog(viewController: UIViewController)
    func showRegisterSuccessDismissDialog(viewController: UIViewController)
    func showWrongPasswordDialog(viewController: UIViewController)
    func showVerificationEmailDismissSent(viewController: UIViewController)
    func showVerificationEmailSent(viewController: UIViewController)
    func showUnknownErrorDialog(viewController: UIViewController)
    
}

class DialogRepositoryImpl: UIViewController, DialogRepository {
    
    func showDeleteFolderDialog(viewController: UIViewController, completion: @escaping() -> Void) {
        let alertController = UIAlertController(title: CDialogTitle.DeleteFolder,
                                                message: CDialogMessage.DeleteFolder, preferredStyle: .alert)
        let deleteAction = UIAlertAction(title: CAction.Delete,
                                         style: .destructive) { _ in
                                            completion()
        }
        let cancelAction = UIAlertAction(title: CAction.Cancel, style: .cancel)
        alertController.addAction(deleteAction)
        alertController.addAction(cancelAction)
        viewController.present(alertController, animated: true, completion: nil)
    }
    
    func showDeleteNoteDialog(viewController: UIViewController, completion: @escaping () -> Void) {
        let alertController = UIAlertController(title: CDialogTitle.DeleteNote,
                                                    message: CDialogMessage.DeleteNote, preferredStyle: .alert)
        let deleteAction = UIAlertAction(title: CAction.Delete,
                                         style: .destructive) { action in
            completion()
        }
        let cancelAction = UIAlertAction(title: CAction.Cancel, style: .cancel)
        alertController.addAction(deleteAction)
        alertController.addAction(cancelAction)
        viewController.present(alertController, animated: true, completion: nil)
    }
    
    func showUserNotVerifiedDialog(viewController: UIViewController, completion: @escaping () -> Void) {
        let alertController = UIAlertController(title: CDialogTitle.EmailNotVerified,
                                                    message: CDialogMessage.EmailNotVerified, preferredStyle: .alert)
        let sendEmailAction = UIAlertAction(title: CAction.Send,
                                         style: .default) { action in
            completion()
        }
        let cancelAction = UIAlertAction(title: CAction.Cancel, style: .cancel)
        alertController.addAction(sendEmailAction)
        alertController.addAction(cancelAction)
        viewController.present(alertController, animated: true, completion: nil)
    }
    
    func showEmailChangedDialog(viewController: UIViewController, completion: @escaping () -> Void) {
        let alert = UIAlertController(title: CDialogTitle.EmailChanged, message: CDialogMessage.EmailChanged, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: CAction.Ok, style: UIAlertAction.Style.default,handler:  { action in
            completion()
        }))
        viewController.present(alert, animated: true, completion: nil)
    }
    
    func showChangePasswordDialog(viewController: UIViewController, completion: @escaping () -> Void) {
        let alert = UIAlertController(title: CDialogTitle.Success, message: CDialogMessage.PasswordChanged, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: CAction.Ok, style: UIAlertAction.Style.default, handler: { action in
            completion()
        }))
        viewController.present(alert, animated: true, completion: nil)
    }
    
    func showNoUserFound(viewController: UIViewController, completion: @escaping() -> Void) {
        let alert = UIAlertController(title: CDialogTitle.Error, message: CDialogMessage.UserNotFound, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: CAction.Ok, style: UIAlertAction.Style.default, handler: { action in
            completion()
        }))
        viewController.present(alert, animated: true, completion: nil)
    }
    
    func showInvalidEmailDialog(viewController: UIViewController) {
        self.showDialog(viewController: viewController, title: CDialogTitle.Error, message: CDialogMessage.InvalidEmail)
    }
    
    func showNetworkErrorDialog(viewController: UIViewController) {
        self.showDialog(viewController: viewController, title: CDialogTitle.Error, message: CDialogMessage.NetworkError)
    }
    
    func showNoAccountErrorDialog(viewController: UIViewController) {
        self.showDialog(viewController: viewController, title: CDialogTitle.Error, message: CDialogMessage.NoAccount)
    }
    
    func showNameTakenDialog(viewController: UIViewController) {
        self.showDialog(viewController: viewController, title: CDialogTitle.Error, message: CDialogMessage.NameTaken)
    }
    
    func showNoEmailFoundDialog(viewController: UIViewController) {
        self.showDialog(viewController: viewController, title: CDialogTitle.Error, message: CDialogMessage.NoEmailFound)
    }
    
    func showWeakPasswordDialog(viewController: UIViewController) {
        self.showDialog(viewController: viewController, title: CDialogTitle.Error, message: CDialogMessage.WeakPassword)
    }
    
    func showEmailAlreadyUsedDialog(viewController: UIViewController) {
        self.showDialog(viewController: viewController, title: CDialogTitle.Error, message: CDialogMessage.EmailAlreadyUsed)
    }
    
    func showTooManyRequestsDialog(viewController: UIViewController) {
        self.showDialog(viewController: viewController, title: CDialogTitle.Error, message: CDialogMessage.TooManyRequests)
    }
    
    func showUpdatePhoneNumberDialog(viewController: UIViewController) {
        self.showDialog(viewController: viewController, title: CDialogTitle.UpdatePhoneNumber, message: CDialogMessage.UpdatePhoneNumber)
    }
    
    func showBadCredentialsDialog(viewController: UIViewController) {
        self.showDialog(viewController: viewController, title: CDialogTitle.Error, message: CDialogMessage.BadCredentials)
    }
    
    func showPasswordDoNotMatchDialog(viewController: UIViewController) {
        self.showDialog(viewController: viewController, title: CDialogTitle.Error, message: CDialogMessage.PasswordsDoNotMatch)
    }
    
    func showUnknownErrorDialog(viewController: UIViewController) {
        self.showDialog(viewController: viewController, title: CDialogTitle.Error, message: "")
    }
    
    func showRegisterSuccessDismissDialog(viewController: UIViewController) {
        self.showViewDismissDialog(viewController: viewController, title: CDialogTitle.AccountCreated, message: CDialogMessage.EmailSent)
    }
    
    func showInvalidEmailDismissDialog(viewController: UIViewController) {
        self.showViewDismissDialog(viewController: viewController, title: CDialogTitle.Error, message: CDialogMessage.InvalidEmail)
    }
    
    func showTryAgainDialog(viewController: UIViewController) {
        self.showViewDismissDialog(viewController: viewController, title: CDialogTitle.Error, message: CDialogMessage.TryAgain)
    }
    
    func showNewPasswordCanNotBeOldDialog(viewController: UIViewController) {
        self.showDialog(viewController: viewController, title: CDialogTitle.Error, message: CDialogMessage.NewPasswordCanNotBeOld)
    }
    
    func showVerificationEmailSent(viewController: UIViewController) {
        self.showDialog(viewController: viewController, title: CDialogTitle.EmailVerificationSent, message: CDialogMessage.EmailSent)
    }
    
    func showWrongPasswordDialog(viewController: UIViewController) {
        self.showDialog(viewController: viewController, title: CDialogTitle.Error, message: CDialogMessage.WrongPassword)
    }
    
    func showTryAgainError(viewController: UIViewController) {
        self.showDialog(viewController: viewController, title: CDialogTitle.Error, message: CDialogMessage.TryAgain)
    }
    
    func showUseNewEmailDialog(viewController: UIViewController) {
        self.showDialog(viewController: viewController, title: CDialogTitle.Error, message: CDialogMessage.TryAgain)
    }
    
    func showVerificationEmailDismissSent(viewController: UIViewController) {
        self.showViewDismissDialog(viewController: viewController, title: CDialogTitle.UseNewEmail, message: "")
    }
    
    func showInvalidPasswordDialog(viewController: UIViewController) {
        self.showViewDismissDialog(viewController: viewController, title: CDialogTitle.Error, message: CDialogMessage.InvalidPassword)
    }
    
    private func showDialog(viewController: UIViewController, title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: CAction.Ok, style: UIAlertAction.Style.default, handler: nil))
        viewController.present(alert, animated: true, completion: nil)
    }
    
    private func showViewDismissDialog(viewController: UIViewController, title: String, message: String) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
    alert.addAction(UIAlertAction(title: CAction.Ok, style: UIAlertAction.Style.default, handler: { _ in
        viewController.dismiss(animated: true, completion: nil)
    }))
    viewController.present(alert, animated: true, completion: nil)
    }
}
